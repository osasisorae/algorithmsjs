// Creates a stack
var Stack = function() {
    this.count = 0;
    this.storage = {};

    // Function to add value to end of stack
    this.push = function(value) {
        this.storage[this.count] = value;
        this.count++;
    }

    // Function to remove and return value at the end of a stack
    this.pop = function() {
        if (this.count === 0) {
            return undefined;
        }

        this.count--;
        var result = this.storage[this.count];
        delete this.storage[this.count];
        return result;
    }

    // Function to return the size of stack
    this.size = function() {
        return this.count;
    }

    // Function that returns the value at the end of the stack
    this.peek = function() {
        return this.storage[this.count-1];
    }

}
// TO TEST

/*
var myStack = new Stack();

myStack.push(1);
myStack.push(2);
console.log(myStack.peek());
console.log(myStack.pop());
console.log(myStack.peek());
*/