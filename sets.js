/* Note: ES6 has an built in se function but we'll create ours */


function mySet() {
    /*  A Set is an unordered collection data type that is iterable, mutable and has no duplicate elements */

    // this collection holds the set
    var collection = [];
    // a methof that checks for the presence of an element and return true or false
    this.has = function(element) {
        return (collection.indexOf(element) !== -1);
    };
    // this method returns all the values in the set
    this.values = function() {
        return collection;
    };
    // this method adds an element to the set
    this.add = function(element) {
        if(!this.has(element)) {
            collection.push(element);
            return true;
        }
        return false;
    };
    // this method removes an element from a set
    this.remove = function(element) {
        if(this.has(element)){
            index = collection.indexOf(element);
            collection.splice(index,1);
            return true;
        }
        return false;
    }
    // this method returns the size of the collection
    this.size = function() {
        return collection.length;
    }
    /* The methods below are not included in ES6 set() but are included in sets in general */

    // this method returns the union of two sets
    this.union = function(otherSet) {
        var unionSet = new mySet();
        var firstSet = this.values();
        var secondSet = otherSet.values();
        firstSet.forEach(function(e){
            unionSet.add(e);
        });
        secondSet.forEach(function(e){
            unionSet.add(e);
        });
        return unionSet;
    };
    // this method will return the intersection of two sets as a new set
    this.intersection = function(otherSet) {
        var intersectionSet = new mySet();
        var firstSet = this.values();
        firstSet.forEach(function(e){
            if(otherSet.has(e)){
                intersectionSet.add(e);
            }
        });
        return intersectionSet;
    };
    // this method will return the difference of two sets as a new set
    this.difference = function(otherSet) {
        var differenceSet = new mySet();
        var firstSet = this.values();
        firstSet.forEach(function(e){
            if(!otherSet.has(e)){
                differenceSet.add(e);
            }
        });
        return differenceSet;
    };
    // this method will test if the set is a subset of a different set
    this.subset = function(otherSet) {
        var firstSet = this.values();
        return firstSet.every(function(value){
            return otherSet.has(value);
        });
    };
}
/* Test our customized mySet() */

// var setA = new mySet();
// var setB = new mySet();
// setA.add("b");
// setA.add("a");
// setB.add("b");
// setB.add("c");
// setB.add("a");
// setB.add("a");
// console.log(setA.subset(setB));
// console.log(setA.intersection(setB).values());

/* Test the built in Set() */

var setC = new Set();
var setD = new Set();
setC.add("b");
setC.add("a");
setD.add("b");
setD.add("c");
setD.add("a");
setD.add("a");
console.log(setC.values());// returns an iterable values in the set
setD.delete("a");
console.log(setD.has("a"));
console.log(setD.add("d"));


/*
console.log(setC.subset(setD));// throws an error
console.log(setC.intersection(setD).values());// throws an error
*/