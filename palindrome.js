
/* Palindrome */

var letters = []; // Our stack

var word = "freeCodeCamp";

var revWord = "";

// Add letters of word to stack
for(let i = 0; i < word.length; i++ ){
    letters.push(word[i]);
}

// pop off the stack in reverse order to create the reverse of the word variable
for (let i = 0; i < word.length; i++) {
    revWord += letters.pop();
}

// To check word is a palindrome
if (revWord === word) {
    console.log(word + "is a palindrome.");
} else {
    console.log(word + "is not a palindrome.")
}
